# 🔑 keys

This repo holds my public keys for various technologies.

## Encrypted files

Wanna send me an encrypted file? Use [age](https://github.com/FiloSottile/age):

```bash
# You can use my keys from gitlab for convenience or get them from the ssh folder in this repo to be sure they are correct
curl -sL https://gitlab.com/xengi.keys | grep ssh-ed25519 | age -R - super_secret.document > super_secret.document.age
cat ./ssh/*.pub | grep ssh-ed25519 | age -R - super_secret.document > super_secret.document.age
```

## Encrypted messages

Wanna send me an encrypted message? Please don't use GPG or S/MIME via email if you can avoid it. Use [matrix](https://matrix.to/#/@xengi:xengi.de) or [signal](https://signal.me/#eu/itt4QAeun95a6RAM0xZIWtU_q52j2rvXJCxlrMtHWPqJCbKOUNFJ9oEZWQBzIRRt) instead.

## Signed files

Files signed using my [minisign](https://jedisct1.github.io/minisign/) key can be verified with the following command:

```bash
minisign -Vm <file> -P RWSx+Fgmna4j7aFnem48FoB5R42rAoAOJAttJD8SOMetCr8D0CFJxMN1
```

## SSH

Wanna give me access to something via SSH? Please add all of my keys. Each of my devices has it's own key, so I can revoke them easily if they ever get stolen.

## No GPG/PGP?

Why?

- https://restoreprivacy.com/let-pgp-die/
- https://blog.cryptographyengineering.com/2014/08/13/whats-matter-with-pgp/

---

Made with ❤️ and 🔐.
